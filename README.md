# gcal-mattermost

## Build

    cd /opt
    git clone git@gitlab.com:ballarat-hackerspace/services/gcal-mattermost.git
    cd gcal-mattermost
    docker build -t gcalcli .

## Setup

Follow the instructions on https://github.com/insanum/gcalcli under the _Login Information_ to create
the required Google API OAUTH credentials. Use the following command to initialise the app after doing
the steps described. If done right you will have a client-id and secret for the next step.

Now run:

    docker run --rm -it -v $(pwd)/conf:/conf gcalcli --config-folder /conf --client-id <client-id> --client-secret <secret> --noauth_local_webserver list

Browse to the URL it gives you, approve the access then copy and paste the code at the final step into the prompt.

The command is now authenticated and can be run without the client-id or secret any further.

Copy the file `config.ini.example` to `config.ini` and then edit it to suit.

## Installation

Manually run the command `./send_report.sh` and ensure the expected message is sent to Mattermost and
then install and activate the service and timer:

    sudo systemctl enable $(pwd)/gcal-mattermost.service
    sudo systemctl enable --now $(pwd)/gcal-mattermost.timer
