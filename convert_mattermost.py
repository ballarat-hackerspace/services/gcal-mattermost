#!/usr/bin/env python3

import re
import sys
import arrow

def shorten_urls(text):
    # Regex pattern to match URLs
    url_pattern = re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')

    urls = url_pattern.findall(text)
    for url in urls:
        short = url.split('://')[1]
        if len(short) > 14:
            short = f'{short.split("/")[0]}/{short.split("/")[1][:6]}...'
        markdown = f'[{short}]({url})'
        text = text.replace(url, markdown)

    return text

today = arrow.now()
tomorrow = today.shift(days=1)

print('### Upcoming Calendar Events\n\n| Day | Time | Event |\n| --- | --- | --- |')

try:
    for line in sys.stdin:
        values = line.rstrip().split('\t')
        start = arrow.get('%s %s' % (values[0], values[1]))
        end = arrow.get('%s %s' % (values[2], values[3]))
        if start.date() == today.date():
            day = 'Today'
        elif start.date() == tomorrow.date():
            day = 'Tomorrow'
        else:
            day = start.humanize()

        # All day events have same start and end time (exact 24 hour entry)
        if start.strftime("%-I%p") == end.strftime("%-I%p"):
            time = 'Reminder'
            # humanize reminders to have an 11am start time
            day = start.shift(hours=11).humanize()
        else:
            time = f'{start.strftime("%-I%p")}-{end.strftime("%-I%p")}'

        print('| %s (%s) | %s | %s |' % (start.strftime("%a"), day, time, shorten_urls(values[4])))
except Exception as e:
    print('| %s | %s | %s |' % ('ERROR', type(e), e))

if len(sys.argv) > 1 and len(sys.argv[1]) > 0:
    print('\n##### %s' % sys.argv[1])
