FROM debian:11-slim

RUN set -x \
	&& apt-get update \
	&& apt-get install -y gcalcli python3-setuptools \
	&& rm -rf /var/lib/apt/lists/*

RUN set -x \
	&& ln -sf /usr/share/zoneinfo/Australia/Melbourne /etc/localtime

ENTRYPOINT [ "gcalcli" ]

