#!/bin/bash

hostname=$(awk -F "=" '/mattermost_hostname/ {print $2}' config.ini)
channel=$(awk -F "=" '/mattermost_channel/ {print $2}' config.ini)
api_path=$(awk -F "=" '/mattermost_path/ {print $2}' config.ini)
icon=$(awk -F "=" '/mattermost_icon_url/ {print $2}' config.ini)

if [ "$#" -ge 2 ]; then
  channel=$1
  calendar=$2
  note=$3
fi

if [ -z "${calendar}" ]; then
  report=$(docker run --rm -v $(pwd)/conf:/conf gcalcli --config-folder /conf --lineart fancy --nocolor agenda --military --tsv | sed 's/\r$//')
else
  report=$(docker run --rm -v $(pwd)/conf:/conf gcalcli --config-folder /conf --lineart fancy --nocolor --calendar "${calendar}" agenda --military --tsv | sed 's/\r$//')
fi

if [ ! -z "${report}" ]; then
  section=$(./convert_mattermost.py "${note}" <<< "$report")
  payload="{ \"channel\": \"$channel\", \"username\": \"Calendar Reminder\", \"icon_url\": \"$icon\", \"text\": \"$section\" }"
  curl -X POST --data-urlencode "payload=$payload" https://$hostname/$api_path
fi
